# Gitlab Proof

This is an OpenPGP proof that connects [my OpenPGP key](https://keyoxide.org/5119C11D57D14127ACA1B6CFEAB09A2560EC3BC6) to this GitLab account. For details check out https://keyoxide.org/guides/openpgp-proofs.

[Verifying my OpenPGP key: openpgp4fpr:5119C11D57D14127ACA1B6CFEAB09A2560EC3BC6]
